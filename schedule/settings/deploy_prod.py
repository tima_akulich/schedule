from schedule.settings.deploy_base import *

CURRENT_ENV = 'prod'

HOST = 'xx.xx.xx.xx'
KEY_FILENAME = 'xxxxxx.pem'

DB_HOST = 'xxxxxx.rds.amazonaws.com'
DB_USER = 'xxxxxx'
DB_PASSWORD = 'xxxxxxxxxx'

AWS_STORAGE_BUCKET_NAME = 'xxxxxx'
AWS_ACCESS_KEY_ID = 'xxxxxxxxxx'
AWS_SECRET_ACCESS_KEY = 'xxxxxxxxxxxxxxxxxxxxxx'

SETTINGS_MODULE = PROJECT_NAME + '.settings.prod'

GUNI_PORT = '8001'
GUNI_WORKERS = 4

CELERYD_WORKERS = 8