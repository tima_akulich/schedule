"""
Django settings for schedule project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from django.core.urlresolvers import reverse_lazy

from kombu import Exchange, Queue


BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = r"^!i*uy!^(!!^xfl99bzshm2(&5vp8#+p%9b$kwl@bphua8&i02"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'djcelery',
    'base',
    'i18next',
    'social.apps.django_app.default',
    'crispy_forms',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'solid_i18n.middleware.SolidLocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'base.middleware.CanonicalDomainMiddleware',
)

ROOT_URLCONF = 'schedule.urls'

WSGI_APPLICATION = 'schedule.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DB_USER = os.environ['DB_USER']
DB_PASSWORD = os.environ['DB_PASSWORD']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'schedule',
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': 'localhost',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

from django.utils.translation import ugettext_lazy as _
LANGUAGES = (
    ('en', _('English')),
    ('ru', _('Russian')),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
    'social.apps.django_app.context_processors.backends',
    'social.apps.django_app.context_processors.login_redirect',
    # "base.context_processors.get_current_url_name",
)

# Celery
USE_CELERY = False
if USE_CELERY:
    import djcelery
    djcelery.setup_loader()
    CELERY_SEND_TASK_ERROR_EMAILS = True
    BROKER_URL = 'amqp://guest@localhost//'
    CELERY_DEFAULT_QUEUE = 'schedule-queue'
    CELERY_DEFAULT_EXCHANGE = 'schedule-queue'
    CELERY_DEFAULT_ROUTING_KEY = 'schedule-queue'
    CELERY_QUEUES = (
        Queue('schedule-queue', Exchange('schedule-queue'), routing_key='schedule-queue'),
    )

# Grappelli customization
GRAPPELLI_ADMIN_TITLE = 'schedule'


# S3 stuff
AWS_QUERYSTRING_AUTH = False
AWS_S3_SECURE_URLS = False


# Compressor & Cloudfront settings
USE_COMPRESSOR = False
USE_CLOUDFRONT = False

if USE_CLOUDFRONT or USE_COMPRESSOR:
    AWS_HEADERS = {'Cache-Control': str('public, max-age=604800')}

if USE_COMPRESSOR:
    ########## COMPRESSION CONFIGURATION
    INSTALLED_APPS += ('compressor',)
    STATICFILES_FINDERS += ('compressor.finders.CompressorFinder',)

    # See: http://django_compressor.readthedocs.org/en/latest/settings/#django.conf.settings.COMPRESS_ENABLED
    COMPRESS_ENABLED = True

    # See: http://django-compressor.readthedocs.org/en/latest/settings/#django.conf.settings.COMPRESS_CSS_HASHING_METHOD
    COMPRESS_CSS_HASHING_METHOD = 'content'

    COMPRESS_CSS_FILTERS = (
        'schedule.settings.abs_compress.CustomCssAbsoluteFilter',
        'compressor.filters.cssmin.CSSMinFilter'
    )

    COMPRESS_OFFLINE = True
    COMPRESS_OUTPUT_DIR = "cache"
    COMPRESS_CACHE_BACKEND = "locmem"
    ########## END COMPRESSION CONFIGURATION

AUTH_USER_MODEL = 'base.ApplicationUser'
SOCIAL_AUTH_USER_MODEL = 'base.ApplicationUser'

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    # 'stellar_client.main.backends.VerifyEmailBackend',
    'social.backends.google.GoogleOAuth2',
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.vk.VKOAuth2'
)

LOGIN_URL = reverse_lazy('auth')
LOGIN_REDIRECT_URL = reverse_lazy('user_profile_overview_me')

CRISPY_TEMPLATE_PACK = 'bootstrap3'

MAX_LESSONS_PER_DAY = 1
MAX_LECTURES_PER_DAY = 2
