from schedule.settings.deploy_base import *

CURRENT_ENV = 'staging'

# This is params for common EC2 instance
HOST = '46.101.226.190'
KEY_FILENAME = 'common.pem'

# This is params for common RDS instance
DB_HOST = 'localhost'
DB_USER = 'postgres'
DB_PASSWORD = 'timaportfolio'

AWS_STORAGE_BUCKET_NAME = 'xxxxxx'
AWS_ACCESS_KEY_ID = 'xxxxxx'
AWS_SECRET_ACCESS_KEY = 'xxxxxx'

SETTINGS_MODULE = PROJECT_NAME + '.settings.staging'

GUNI_PORT = '8002'
GUNI_WORKERS = 2

CELERYD_WORKERS = 1
