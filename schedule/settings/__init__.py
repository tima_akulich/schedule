import os

ENV = os.environ.get('ENV', 'PROD').upper()
DEV = ENV == 'DEV'
STAGING = ENV == 'STAGING'
PROD = ENV == 'PROD'

if DEV:
    from schedule.settings.dev import *
elif STAGING:
    from schedule.settings.staging import *
else:
    from schedule.settings.prod import *