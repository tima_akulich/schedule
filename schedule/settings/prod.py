from schedule.settings.base import *
from schedule.settings import deploy_prod as deploy
from schedule.settings.deploy_prod import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_STORAGE_BUCKET_NAME

PROD = True
DEV = False
STAGING = False
ENV = 'PROD'

DEBUG = TEMPLATE_DEBUG = False

ADMINS = (
    ('Tima Akulich', 'tima.akulich@gmail.com'),
)
MANAGERS = ADMINS

CURRENT_HOST = "XXXXXXXXXX"
BASE_URL = "http://" + CURRENT_HOST
ALLOWED_HOSTS = [deploy.HOST, 'localhost', '.' + CURRENT_HOST]

DATABASES['default']['HOST'] = deploy.DB_HOST
DATABASES['default']['PORT'] = '5432'


if USE_CLOUDFRONT:
    AWS_S3_CUSTOM_DOMAIN = 'static.%s' % CURRENT_HOST
    S3_URL = 'http://%s/' % AWS_S3_CUSTOM_DOMAIN
else:
    S3_URL = 'http://%s.s3.amazonaws.com/' % deploy.AWS_STORAGE_BUCKET_NAME

STATIC_URL = S3_URL + 'static/'
MEDIA_URL = S3_URL + 'media/'

DEFAULT_FILE_STORAGE = 'schedule.settings.s3utils.MediaRootS3BotoStorage'
STATICFILES_STORAGE = 'schedule.settings.s3utils.StaticRootS3BotoStorage'

COMPRESS_STORAGE = STATICFILES_STORAGE

# Email settings (Mandrill)
DEFAULT_FROM_EMAIL = 'noreply@%s' % CURRENT_HOST
SERVER_EMAIL = DEFAULT_FROM_EMAIL

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = '######'
EMAIL_HOST_PASSWORD = '#######'
