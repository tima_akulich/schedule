from schedule.settings.base import *
from schedule.settings import deploy_staging as deploy
from schedule.settings.deploy_staging import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_STORAGE_BUCKET_NAME

STAGING = True
DEV = False
PROD = False
ENV = 'STAGING'

DEBUG = TEMPLATE_DEBUG = False

ADMINS = (
    ('Tima Akulich', 'tima.akulich@gmail.com'),
)

CURRENT_HOST = 'schedule.timophey.xyz'
BASE_URL = "http://" + CURRENT_HOST
ALLOWED_HOSTS = [deploy.HOST, '.' + CURRENT_HOST]

DATABASES['default']['HOST'] = deploy.DB_HOST
DATABASES['default']['PORT'] = '5432'

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

# Email settings (Mandrill)
DEFAULT_FROM_EMAIL = 'noreply@%s' % CURRENT_HOST
SERVER_EMAIL = DEFAULT_FROM_EMAIL

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'tima.akulich@gmail.com'
EMAIL_HOST_PASSWORD = 'fw0XRhXqxAIaBUl9b35Ezw'
