# First production deploy

1. Create EC2 instance based on "Ubuntu". Save permission key to the project directory. In file schedule/settings/deploy_prod.py set KEY_FILENAME params.

2. Associate Elastic IP address with created EC2 instance. Set HOST param by this address. Register domain name and put it to CURRENT_HOST param in the schedule/settings/prod.py.

3. Create RDS instance based on PostgreSQL. Set DB_HOST, DB_USER and DB_PASSWORD params in schedule/settings/deploy_prod.py.

4. Create S3 bucket and set AWS_STORAGE_BUCKET_NAME, AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY params.

5. Execute followed command:
    - fab prepare
    - fab config:False
    - fab deploy
