import itertools

class Classroom(object):
    def __init__(self, number, time):
        self.number = number
        self.time = time

    def __unicode__(self):
        return u"#:{0} - {1}".format(self.number, self.time)

ROOMS = [1, 2, 3]
TIME = [8, 10, 12, 14]

for i, j in itertools.product(ROOMS, TIME):
    print Classroom(i, j).__unicode__()