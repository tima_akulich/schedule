# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20160424_1344'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='groupsubject',
            name='group',
        ),
        migrations.AddField(
            model_name='groupsubject',
            name='groups',
            field=models.ManyToManyField(related_name='group_subjects', verbose_name='Group', to='base.Group'),
        ),
    ]
