# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import django.contrib.auth.models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Classroom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.CharField(unique=True, max_length=5, verbose_name='Number of room')),
                ('type', models.SmallIntegerField(default=None, null=True, verbose_name='Type of room', choices=[(1, 'Practice'), (0, 'Lecture'), (2, 'Machine')])),
            ],
            options={
                'verbose_name': 'Classroom',
                'verbose_name_plural': 'Classrooms',
            },
        ),
        migrations.CreateModel(
            name='FreeClassRoom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('day_of_week', models.CharField(default=None, max_length=10, null=True, verbose_name='Day of the week', choices=[(b'monday', 'Monday'), (b'tuesday', 'Tuesday'), (b'wednesday', 'Wednesday'), (b'thursday', 'Thursday'), (b'friday', 'Friday'), (b'saturday', 'Saturday')])),
                ('is_free', models.BooleanField(default=True)),
                ('class_room', models.ForeignKey(verbose_name='Classroom', to='base.Classroom')),
            ],
            options={
                'verbose_name': 'Free class room',
                'verbose_name_plural': 'Free class rooms',
            },
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=10, verbose_name='Name')),
                ('course', models.PositiveIntegerField(verbose_name='Course')),
            ],
            options={
                'verbose_name': 'Group',
                'verbose_name_plural': 'Groups',
            },
        ),
        migrations.CreateModel(
            name='GroupSubject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.SmallIntegerField(default=None, null=True, verbose_name='Type', choices=[(1, 'Practice'), (0, 'Lecture'), (2, 'Machine')])),
                ('count', models.PositiveIntegerField(verbose_name='Lessons per week')),
                ('group', models.ForeignKey(verbose_name='Group', to='base.Group')),
            ],
            options={
                'verbose_name': 'Group subject',
                'verbose_name_plural': 'Group subjects',
            },
        ),
        migrations.CreateModel(
            name='Lesson',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('free_classroom', models.ForeignKey(verbose_name=b'Free classroom', to='base.FreeClassRoom')),
                ('group_subjects', models.ManyToManyField(related_name='lessons', verbose_name=b'Group subjects', to='base.GroupSubject')),
            ],
            options={
                'verbose_name': 'Lesson',
                'verbose_name_plural': 'Lessons',
            },
        ),
        migrations.CreateModel(
            name='LessonTime',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.TimeField(unique=True, verbose_name='Time')),
            ],
            options={
                'verbose_name': 'Lesson time',
                'verbose_name_plural': 'Lesson times',
            },
        ),
        migrations.CreateModel(
            name='Subject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='Subject name')),
            ],
            options={
                'verbose_name': 'Subject',
                'verbose_name_plural': 'Subjects',
            },
        ),
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='Teacher name')),
            ],
            options={
                'verbose_name': 'Teacher',
                'verbose_name_plural': 'Teachers',
            },
        ),
        migrations.AlterModelOptions(
            name='applicationuser',
            options={'verbose_name': 'user', 'verbose_name_plural': 'users'},
        ),
        migrations.AlterModelManagers(
            name='applicationuser',
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.RemoveField(
            model_name='applicationuser',
            name='email_code',
        ),
        migrations.RemoveField(
            model_name='applicationuser',
            name='is_admin',
        ),
        migrations.RemoveField(
            model_name='applicationuser',
            name='is_email_verified',
        ),
        migrations.AddField(
            model_name='applicationuser',
            name='groups',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='applicationuser',
            name='is_staff',
            field=models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status'),
        ),
        migrations.AddField(
            model_name='applicationuser',
            name='is_superuser',
            field=models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status'),
        ),
        migrations.AddField(
            model_name='applicationuser',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
        ),
        migrations.AddField(
            model_name='applicationuser',
            name='username',
            field=models.CharField(default='', error_messages={'unique': 'A user with that username already exists.'}, max_length=30, validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Enter a valid username. This value may contain only letters, numbers and @/./+/-/_ characters.', 'invalid')], help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', unique=True, verbose_name='username'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='applicationuser',
            name='date_joined',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined'),
        ),
        migrations.AlterField(
            model_name='applicationuser',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='email address', blank=True),
        ),
        migrations.AlterField(
            model_name='applicationuser',
            name='first_name',
            field=models.CharField(default='', max_length=30, verbose_name='first name', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='applicationuser',
            name='is_active',
            field=models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active'),
        ),
        migrations.AlterField(
            model_name='applicationuser',
            name='last_login',
            field=models.DateTimeField(null=True, verbose_name='last login', blank=True),
        ),
        migrations.AlterField(
            model_name='applicationuser',
            name='last_name',
            field=models.CharField(default='', max_length=30, verbose_name='last name', blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='groupsubject',
            name='subject',
            field=models.ForeignKey(verbose_name='Subject', to='base.Subject'),
        ),
        migrations.AddField(
            model_name='groupsubject',
            name='teacher',
            field=models.ForeignKey(verbose_name='Teacher', to='base.Teacher'),
        ),
        migrations.AlterUniqueTogether(
            name='group',
            unique_together=set([('name', 'course')]),
        ),
        migrations.AddField(
            model_name='freeclassroom',
            name='time',
            field=models.ForeignKey(verbose_name='Time', to='base.LessonTime'),
        ),
        migrations.AlterUniqueTogether(
            name='freeclassroom',
            unique_together=set([('class_room', 'time', 'day_of_week')]),
        ),
    ]
