# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0003_auto_20160424_1404'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lesson',
            name='group_subjects',
        ),
        migrations.AddField(
            model_name='lesson',
            name='group_subject',
            field=models.ForeignKey(default=None, verbose_name=b'Group subject', to='base.GroupSubject', null=True),
        ),
    ]
