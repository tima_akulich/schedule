# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0004_auto_20160424_1423'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='teacher',
            name='name',
        ),
        migrations.AddField(
            model_name='subject',
            name='description',
            field=models.TextField(verbose_name='Description', blank=True),
        ),
        migrations.AddField(
            model_name='teacher',
            name='description',
            field=models.TextField(verbose_name='Description', blank=True),
        ),
        migrations.AddField(
            model_name='teacher',
            name='first_name',
            field=models.CharField(max_length=100, null=True, verbose_name='First name'),
        ),
        migrations.AddField(
            model_name='teacher',
            name='last_name',
            field=models.CharField(max_length=100, null=True, verbose_name='Last name'),
        ),
        migrations.AddField(
            model_name='teacher',
            name='middle_name',
            field=models.CharField(max_length=100, null=True, verbose_name='Middle name'),
        ),
        migrations.AddField(
            model_name='teacher',
            name='photo',
            field=models.ImageField(default=None, upload_to=b'', null=True, verbose_name='Photo', blank=True),
        ),
    ]
