# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0005_auto_20160424_1644'),
    ]

    operations = [
        migrations.AddField(
            model_name='subject',
            name='short_name',
            field=models.CharField(max_length=20, null=True, verbose_name='Short name', blank=True),
        ),
    ]
