# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0007_auto_20160508_1022'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='group',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='group',
            name='course',
        ),
    ]
