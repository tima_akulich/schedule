# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0006_subject_short_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.PositiveIntegerField(unique=True, verbose_name='Number')),
            ],
            options={
                'verbose_name': 'Course',
                'verbose_name_plural': 'Courses',
            },
        ),
        migrations.CreateModel(
            name='Shift',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=10, verbose_name='Name')),
                ('times', models.ManyToManyField(to='base.LessonTime', verbose_name='Times')),
            ],
            options={
                'verbose_name': 'Shift',
                'verbose_name_plural': 'Shifts',
            },
        ),
        migrations.AddField(
            model_name='course',
            name='shift',
            field=models.ForeignKey(verbose_name='Shift', to='base.Shift'),
        ),
        migrations.AddField(
            model_name='group',
            name='course_fk',
            field=models.ForeignKey(verbose_name='Course', to='base.Course', null=True),
        ),
    ]
