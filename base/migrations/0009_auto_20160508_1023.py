# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0008_auto_20160508_1023'),
    ]

    operations = [
        migrations.RenameField(
            model_name='group',
            old_name='course_fk',
            new_name='course',
        ),
        migrations.AlterUniqueTogether(
            name='group',
            unique_together=set([('name', 'course')]),
        ),
    ]
