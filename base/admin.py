from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from base.models import ApplicationUser, Classroom, LessonTime, FreeClassRoom, Group, Teacher, Subject, GroupSubject, \
    Lesson, Shift, Course

admin.site.register(ApplicationUser)
admin.site.register(Classroom)
admin.site.register(LessonTime)
admin.site.register(FreeClassRoom)
admin.site.register(Shift)
admin.site.register(Course)
admin.site.register(Group)
admin.site.register(GroupSubject)
admin.site.register(Lesson)


class TeacherAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'last_name', 'first_name', 'middle_name', 'photo_html', 'description')
    search_fields = ('first_name', 'last_name', 'middle_name')

    def photo_html(self, obj):
        return u"<img src='{0}' style='max-height: 100px'/>".format(obj.photo.url)
    photo_html.allow_tags = True
    photo_html.short_description = _('Photo')


class SubjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'short_name', 'description')
    search_fields = ('name', 'short_name')

admin.site.register(Teacher, TeacherAdmin)
admin.site.register(Subject, SubjectAdmin)

