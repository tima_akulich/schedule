from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from base.models import Classroom, LessonTime, FreeClassRoom, DAYS


@receiver(post_save, sender=Classroom)
def generate_free_classrooms_1(sender, instance, created, **kwargs):
    if created:
        times = LessonTime.objects.all()
        free_classrooms = []
        for time in times:
            for day in DAYS.CHOICES:
                free_classrooms.append(FreeClassRoom(class_room=instance, time=time, day_of_week=day[0]))
        FreeClassRoom.objects.bulk_create(free_classrooms)


@receiver(pre_delete, sender=Classroom)
def remove_old_free_classrooms_1(sender, instance, **kwargs):
    FreeClassRoom.objects.filter(class_room=instance).delete()


@receiver(post_save, sender=LessonTime)
def generate_free_classrooms_2(sender, instance, created, **kwargs):
    if created:
        classrooms = Classroom.objects.all()
        free_classrooms = []
        for classroom in classrooms:
            for day in DAYS.CHOICES:
                free_classrooms.append(FreeClassRoom(class_room=classroom, time=instance, day_of_week=day[0]))
        FreeClassRoom.objects.bulk_create(free_classrooms)


@receiver(pre_delete, sender=LessonTime)
def remove_old_free_classrooms_2(sender, instance, **kwargs):
    FreeClassRoom.objects.filter(time=instance).delete()

