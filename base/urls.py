from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView
from base.views.auth import SignInView, SignUpView
from base.views.core import ScheduleView, GenerateView

urlpatterns = patterns('',
   url(r'^logout/$', 'django.contrib.auth.views.logout',
                    {'next_page': reverse_lazy('index')}, name='logout')
)

urlpatterns += patterns('base.views',
   url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
   url(r'^sign-in/?$', SignInView.as_view(), name='sign-in'),
   url(r'^sign-up/?$', SignUpView.as_view(), name='sign-up'),
   url(r'^g/?$', GenerateView.as_view(), name='generate'),
   url(r'^schedule/(?P<course>\w+)/?$', ScheduleView.as_view(), name='schedule'),
   url(r'^lesson/(?P<lesson>\w+)/?$', ScheduleView.as_view(), name='lesson'),
   # url(r'^me/$', UserProfileOverviewMe.as_view(), name='user_profile_overview_me'),
   # url(r'^(?P<user>[\w\.]+)/?$', UserProfileOverview.as_view(), name='user_profile_overview'),
   # url(r'^me/settings/$', UserProfileSettings.as_view(), name='user_profile_settings'),
)


