import os
from django.conf import settings
from django.http import Http404


def merge_dicts(a, b):
    if not a:
        return b
    a.update(b)
    return a

def log(to_print):
    path = 'logs'
    if not os.path.exists(path):
        os.makedirs(path)

    with open(os.path.join(path, 'mail_log.log'), 'a') as temp_file:
        temp_file.write(to_print)

def templated_mail_send(to, template_filename, context_dict,
                        from_email=settings.DEFAULT_FROM_EMAIL):

    from templated_email import send_templated_mail
    send_templated_mail(template_name=template_filename, from_email=from_email, recipient_list=[to],
                        context=merge_dicts(context_dict, {"settings": settings}), fail_silently=False)


def ajax_required(f):
    def wrap(request, *args, **kwargs):
            if not request.is_ajax():
                raise Http404
            return f(request, *args, **kwargs)
    wrap.__doc__=f.__doc__
    wrap.__name__=f.__name__
    return wrap

