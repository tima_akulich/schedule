from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.http.response import JsonResponse
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, RedirectView
from django.conf import settings
from django.views.generic.base import View

from base.forms import FormApplicationUserSignin, FormApplicationUserSignup
from base.logic import generate_schedule, get_lesson
from base.models import DAYS, Group, LessonTime, Lesson
from jinja2 import Environment, PackageLoader

jinja2_env = Environment(loader=PackageLoader('base', 'templates'))


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class AuthorizationView(TemplateView):
    template_name = 'auth.html'

    def get_context_data(self, **kwargs):
        context = {
            'signin_form': FormApplicationUserSignin(),
            'signup_form': FormApplicationUserSignup()
        }
        return super(AuthorizationView, self).get_context_data(**context)

    def post(self, request, *args, **kwargs):
        is_signin_form = request.POST.get('is_signin_form')
        if bool(is_signin_form):
            form = FormApplicationUserSignin(request.POST, request=request)
            form_name = 'signin_form'
        else:
            form = FormApplicationUserSignup(request.POST, request=request)
            form_name = 'signup_form'
        if form.is_valid():
            form.save()
            return redirect(settings.LOGIN_REDIRECT_URL)
        context = self.get_context_data()
        context[form_name] = form
        return self.render_to_response(context)


class UserProfileRedirectView(LoginRequiredMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        user = self.request.user
        return reverse('user_profile_overview', kwargs={'user': user.username or 'id%s' % user.id})


class ScheduleView(TemplateView):
    template_name = 'schedule.html'

    def dispatch(self, request, *args, **kwargs):
        self.course = kwargs.get('course')
        return super(ScheduleView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ScheduleView, self).get_context_data(**kwargs)
        context['course'] = self.course
        context['DAYS'] = DAYS.CHOICES
        groups = Group.objects.filter(course__number=self.course)
        groups = sorted(list(groups), key=lambda x: int(x.name) if x.name.isdigit() else -1)
        context['group_range'] = xrange(len(groups))
        context['groups'] = groups
        context['lesson_times'] = LessonTime.objects.all().order_by('time')
        context['STATIC_URL'] = settings.STATIC_URL
        context['get_lesson'] = get_lesson
        context['groups_count'] = len(groups)
        return context

    def get(self, request, *args, **kwargs):
        template = jinja2_env.get_template(self.template_name)
        return HttpResponse(
            template.render(**self.get_context_data())
        )

    # @method_decorator(csrf_exempt)
    def delete(self, request, *args, **kwargs):
        lesson_id = kwargs.get('lesson')
        Lesson.objects.filter(id=lesson_id).delete()
        return JsonResponse(data={"status": "ok"})


class GenerateView(View):
    def get(self, request):
        generate_schedule()
        return HttpResponse('Ok')


