from django.contrib.auth import authenticate, login
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.generic import View
from django.utils.translation import ugettext as _


from base.models import ApplicationUser
from base.utils import ajax_required


class SignInView(View):
    http_method_names = ['post']

    @method_decorator(ajax_required)
    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = ApplicationUser.objects.filter(username=username).first()
        if not user or not user.check_password(password):
            return JsonResponse({'message': _('Invalid username or password')}, status=400)
        user = authenticate(username=username, password=password)
        login(self.request, user)
        return JsonResponse({'message': reverse('index')})


class SignUpView(View):
    http_method_names = ['post']

    @method_decorator(ajax_required)
    def post(self, request):
        username = request.POST.get('username')
        password1 = request.POST.get('password1')
        password2 = request.POST.get('password2')
        if not username:
            return JsonResponse({'message': _("Invalid username")}, status=400)
        if not password1 or password1 != password2:
            return JsonResponse({'message': _("Invalid passwords")}, status=400)
        if ApplicationUser.objects.filter(username=username).exists():
            return JsonResponse({'message': _('This username already in use')}, status=400)
        user = ApplicationUser.objects.create(username=username)
        user.set_password(password1)
        user.save()
        user = authenticate(username=username, password=password1)
        login(self.request, user)
        return JsonResponse({'message': reverse('index')})