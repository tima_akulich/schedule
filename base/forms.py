from crispy_forms.bootstrap import FormActions, StrictButton
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, HTML, Div
from django import forms
from django.utils.translation import ugettext as _
from django.contrib.auth import authenticate, login
from base.models import ApplicationUser


class FormApplicationUserSignin(forms.Form):

    email = forms.EmailField(widget=forms.EmailInput())
    password = forms.CharField(widget=forms.PasswordInput())
    is_signin_form = forms.CharField(widget=forms.HiddenInput(attrs={'value': 'true'}))

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(self.__class__, self).__init__(*args, **kwargs)
        self.fields['email'].label = _('Email')
        self.fields['password'].label = _('Password')
        self.helper = FormHelper()
        self.helper.layout = Layout(FormActions(
            'is_signin_form',
            Field('email', css_class=''),
            Div(
                Field('password', css_class='password-input'),
                HTML("<a href='#'><span class='label label-primary'>%s</span></a>" % _('Forgot password?')),
            css_class='forgot-password'),
            StrictButton(_('Submit'), type='submit', css_class='btn btn-primary')
        ))

    def clean(self):
        cleaned_data = self.cleaned_data
        email = cleaned_data.get('email')
        user = ApplicationUser.objects.filter(email=email).first()
        if not user or not user.check_password(cleaned_data.get('password')):
            self.add_error('email', _('Invalid email or password'))
            self.add_error('password', '')
        return cleaned_data

    def save(self):
        user = authenticate(email=self.cleaned_data['email'], password=self.cleaned_data['password'])
        login(self.request, user)


class FormApplicationUserSignup(forms.Form):

    email = forms.EmailField(widget=forms.EmailInput())
    password = forms.CharField(widget=forms.PasswordInput())
    password_confirmation = forms.CharField(widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(self.__class__, self).__init__(*args, **kwargs)
        self.fields['email'].label = _('Email')
        self.fields['password'].label = _('Password')
        self.fields['password_confirmation'].label = _('Re-type password')
        self.helper = FormHelper()
        self.helper.layout = Layout(FormActions(
            Field('email', css_class=''),
            Field('password', css_class=''),
            Field('password_confirmation', css_class=''),
            StrictButton(_('Submit'), type='submit', css_class='btn btn-primary')
        ))

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if ApplicationUser.objects.filter(email=email).exists():
            raise forms.ValidationError(_('This email already in use'))
        return email

    def clean(self):
        cleaned_data = self.cleaned_data
        password = cleaned_data.get('password')
        password_confirmation = cleaned_data.get('password_confirmation')
        if password != password_confirmation:
            self.add_error('password', _('Passwords don\'t match'))
            self.add_error('password_confirmation', '')
        return cleaned_data

    def save(self):
        data = self.cleaned_data
        user = ApplicationUser.objects.create(email=data['email'])
        user.set_password(data['password'])
        user.save()
        user = authenticate(email=data['email'], password=data['password'])
        login(self.request, user)


class ApplicationUserUpdateForm(forms.ModelForm):
    class Meta:
        model = ApplicationUser
        fields = ('first_name', 'last_name', 'email', 'username')

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(FormActions(
            Field('first_name', css_class=''),
            Field('last_name', css_class=''),
            Field('email', css_class=''),
            Field('username', css_class=''),
            StrictButton(_('Save'), type='submit', css_class='btn btn-primary')
        ))