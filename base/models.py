import base64
import hashlib
import json
from urlparse import urljoin
from django.contrib import auth
from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.utils import timezone
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from base.utils import templated_mail_send, merge_dicts


class ApplicationUser(AbstractUser):

    def send_email(self, template, context=None, from_email=settings.DEFAULT_FROM_EMAIL):
        """
        Sends an email to this User.
        """
        templated_mail_send(self.email, template, merge_dicts(context, {"user": self}), from_email=from_email)

    def send_verify_email(self):
        email_hash = hashlib.sha256()
        email_hash.update(self.email)
        self.email_code = email_hash.hexdigest()
        self.save()
        url = urljoin(settings.BASE_URL, reverse("verify_email", kwargs={"verify_code": self.email_code}))
        self.send_email('activation_email', context={'url': url, 'date_now': timezone.now(), 'email': self.email})


class DAYS(object):
    MONDAY = 'monday'
    TUESDAY = 'tuesday'
    WEDNESDAY = 'wednesday'
    THURSDAY = 'thursday'
    FRIDAY = 'friday'
    SATURDAY = 'saturday'

    CHOICES = (
        # (SUNDAY, _('Sunday')),
        (MONDAY, _('Monday')),
        (TUESDAY, _('Tuesday')),
        (WEDNESDAY, _('Wednesday')),
        (THURSDAY, _('Thursday')),
        (FRIDAY, _('Friday')),
        (SATURDAY, _('Saturday')),
    )


class CLASSROOM(object):
    LECTURE = 0
    PRACTICE = 1
    MACHINE = 2

    CHOICES = (
        (PRACTICE, _('Practice')),
        (LECTURE, _('Lecture')),
        (MACHINE, _('Machine'))
    )


class Classroom(models.Model):
    number = models.CharField(max_length=5, verbose_name=_('Number of room'), unique=True)
    type = models.SmallIntegerField(choices=CLASSROOM.CHOICES, null=True,
                                    default=None, verbose_name=_('Type of room'))

    class Meta:
        verbose_name = _('Classroom')
        verbose_name_plural = _('Classrooms')

    def __unicode__(self):
        return u"{0}({1})".format(self.number, self.get_type_display())


class LessonTime(models.Model):
    time = models.TimeField(verbose_name=_('Time'), unique=True)

    class Meta:
        verbose_name = _('Lesson time')
        verbose_name_plural = _('Lesson times')

    def __unicode__(self):
        return u"{0}".format(self.time)[:-3]


class FreeClassRoom(models.Model):
    class_room = models.ForeignKey(Classroom, verbose_name=_('Classroom'))
    time = models.ForeignKey(LessonTime, verbose_name=_('Time'))
    day_of_week = models.CharField(max_length=10,
                                   null=True,
                                   default=None,
                                   choices=DAYS.CHOICES,
                                   verbose_name=_('Day of the week'))
    is_free = models.BooleanField(default=True)

    def __unicode__(self):
        return u"{2}:{0} - #{1}".format(self.time, self.class_room, self.get_day_of_week_display())

    class Meta:
        verbose_name = _('Free class room')
        verbose_name_plural = _('Free class rooms')
        unique_together = ('class_room', 'time', 'day_of_week')


class Shift(models.Model):
    name = models.CharField(max_length=10, verbose_name=_("Name"), unique=True)
    times = models.ManyToManyField(LessonTime, verbose_name=_("Times"))

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Shift')
        verbose_name_plural = _('Shifts')


class Course(models.Model):
    CHOICES = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    )

    number = models.PositiveIntegerField(verbose_name=_('Number'), unique=True, choices=CHOICES)
    shift = models.ForeignKey(Shift, verbose_name=_('Shift'))

    def __unicode__(self):
        return u"{0} {2} {1} {3}".format(self.number, self.shift, _('Course'), _('Shift'))

    class Meta:
        verbose_name = _('Course')
        verbose_name_plural = _('Courses')


class Group(models.Model):
    name = models.CharField(max_length=10, verbose_name=_('Name'))
    course = models.ForeignKey(Course, verbose_name=_('Course'), null=True)

    def __unicode__(self):
        return u"{0} - {1}".format(self.short_name, self.course)

    @property
    def short_name(self):
        return u"{0} {1}".format(self.name, _('Group'))

    class Meta:
        verbose_name = _('Group')
        verbose_name_plural = _('Groups')
        unique_together = ('name', 'course')
        ordering = ('course__number', )


class Teacher(models.Model):
    last_name = models.CharField(max_length=100, verbose_name=_('Last name'), null=True)
    first_name = models.CharField(max_length=100, verbose_name=_('First name'), null=True)
    middle_name = models.CharField(max_length=100, verbose_name=_('Middle name'), null=True)
    description = models.TextField(verbose_name=_('Description'), blank=True)
    photo = models.ImageField(verbose_name=_('Photo'), null=True, blank=True, default=None)

    def __unicode__(self):
        return self.short_name

    @property
    def short_name(self):
        return u"{0} {1}.{2}.".format(self.last_name.title(),
                                     self.first_name.title()[0],
                                     self.middle_name.title()[0])

    @property
    def full_name(self):
        return u"{0} {1} {2}".format(self.last_name.title(),
                                    self.first_name.title(),
                                    self.middle_name.title())
    class Meta:
        verbose_name = _('Teacher')
        verbose_name_plural = _('Teachers')


class Subject(models.Model):
    name = models.CharField(max_length=100, verbose_name=_('Subject name'))
    short_name = models.CharField(max_length=20, verbose_name=_('Short name'), null=True, blank=True)
    description = models.TextField(blank=True, verbose_name=_('Description'))
    #TODO: files support

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Subject')
        verbose_name_plural = _('Subjects')


class GroupSubject(models.Model):
    teacher = models.ForeignKey(Teacher, verbose_name=_('Teacher'))
    subject = models.ForeignKey(Subject, verbose_name=_('Subject'))
    groups = models.ManyToManyField(Group, verbose_name=_('Group'), related_name='group_subjects')
    type = models.SmallIntegerField(null=True, choices=CLASSROOM.CHOICES,
                                    default=None, verbose_name=_('Type'))
    count = models.PositiveIntegerField(verbose_name=_('Lessons per week'))

    def __unicode__(self):
        return u"x{4} {0} - {1} - {2} ({3})".format(unicode(map(lambda x: str(x), self.groups.all())),
                                                     self.subject,
                                                     self.teacher,
                                                     self.get_type_display(),
                                                     self.count)

    @property
    def lessons_count(self):
        # return Lesson.objects.filter(group_subject__groups__in=self.groups.all(),
        #                              group_subject__subject
        #                              group_subject__type=self.type).distinct().count()
        # return self.lessons.all().count()
        return Lesson.objects.filter(group_subject=self).count()

    class Meta:
        verbose_name = _('Group subject')
        verbose_name_plural = _('Group subjects')


class Lesson(models.Model):
    free_classroom = models.ForeignKey(FreeClassRoom, verbose_name='Free classroom')
    group_subject = models.ForeignKey(GroupSubject, verbose_name='Group subject', null=True, default=None)

    def __unicode__(self):
        return u"{0} {1}".format(self.free_classroom, self.group_subject)

    @classmethod
    def can_create_lesson(cls, group_subject, day, time):
        return not cls.objects.filter(Q(group_subject__teacher=group_subject.teacher) |
                                      Q(group_subject__groups__in=group_subject.groups.all()),
                                      free_classroom__time=time,
                                      free_classroom__day_of_week=day).exists()

    @classmethod
    def can_group(cls, group_subject, day, time):
        return not cls.objects.filter(free_classroom__time=time,
                                      free_classroom__day_of_week=day,
                                      group_subject__groups__in=group_subject.groups.all()).exists()

    @classmethod
    def reach_max_lessons_per_day(cls, group_subject, day):
        count = Lesson.objects.filter(group_subject=group_subject, free_classroom__day_of_week=day).count()
        return count >= settings.MAX_LESSONS_PER_DAY

    @classmethod
    def reach_max_lectures_per_day(cls, group_subject, day):
        if group_subject.type != CLASSROOM.LECTURE:
            return False
        count = Lesson.objects.filter(group_subject__type=CLASSROOM.LECTURE,
                                      free_classroom__day_of_week=day,
                                      group_subject__groups__in=group_subject.groups.all()).distinct().count()
        return count >= settings.MAX_LECTURES_PER_DAY

    def to_dict(self):
        resp = {
            'lesson_id': self.id,
            'subject_full_name': self.group_subject.subject.name,
            'subject_short_name': self.group_subject.subject.short_name,
            'subject_description': self.group_subject.subject.description,
            'teacher_full_name': self.group_subject.teacher.full_name,
            'teacher_short_name': self.group_subject.teacher.short_name,
            'teacher_description': self.group_subject.teacher.description,
            'teacher_photo_url': self.group_subject.teacher.photo.url,
            'class_room_number': self.free_classroom.class_room.number,
            'class_room_type': self.free_classroom.class_room.get_type_display(),
            'time': str(self.free_classroom.time),
        }
        resp['base64'] = base64.b64encode(json.dumps(resp))
        return resp

    class Meta:
        verbose_name = _('Lesson')
        verbose_name_plural = _('Lessons')
