$(function () {
    $('.sign-in-errors').hide();
    $('.sign-up-errors').hide();
    $('#sign-in-form').submit(function (e) {
        $('.sign-in-errors').hide();
        e.preventDefault();
        $(this).ajaxSubmit({
            success: function (data) {
                window.location.replace(data.message);
            },
            error: function (data) {
                $('.sign-in-errors').text(data.responseJSON.message).show();
            }
        });
    });
    $('#sign-up-form').submit(function (e) {
        $('.sign-up-errors').hide();
        e.preventDefault();
        $(this).ajaxSubmit({
            success: function (data) {
                window.location.replace(data.message);
            },
            error: function (data) {
                $('.sign-up-errors').text(data.responseJSON.message).show();
            }
        });
    });
});
