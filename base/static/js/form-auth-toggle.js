$('#sign-up-form').hide();
$(function () {
    $('.toggle-link').on('click', function () {
        if ($(this).parent(0).attr('id') === "sign-up-form") {
            $('#sign-up-form').slideUp();
            $('#sign-in-form').slideDown();

        } else {
            $('#sign-in-form').slideUp();
            $('#sign-up-form').slideDown();
        }
    });
});