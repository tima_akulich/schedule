import random
from base.models import GroupSubject, Lesson, FreeClassRoom, DAYS


def generate_schedule():
    DAYS_CHOICES = DAYS.CHOICES
    DAYS_CHOICES = list(DAYS.CHOICES[:-1])
    random.shuffle(DAYS_CHOICES)
    DAYS_CHOICES.append(DAYS.CHOICES[-1])

    Lesson.objects.all().delete()
    FreeClassRoom.objects.all().update(is_free=True)
    group_subjects = GroupSubject.objects.all().order_by('type')
    for group_subject in group_subjects:
        while group_subject.lessons_count < group_subject.count:
            created = False
            for day in DAYS_CHOICES:
                free_rooms = list(FreeClassRoom.objects.filter(time__in=group_subject.groups.all()[0].course.shift.times.all(),
                                                               is_free=True,
                                                               class_room__type=group_subject.type,
                                                               day_of_week=day[0]).order_by('time'))

                # free_rooms = sorted(free_rooms, key=lambda x: x.time)
                for room in free_rooms:
                    if Lesson.can_create_lesson(group_subject, day[0], room.time) \
                            and not Lesson.reach_max_lessons_per_day(group_subject, day[0])\
                                and not Lesson.reach_max_lectures_per_day(group_subject, day[0]):
                        lesson = Lesson.objects.create(free_classroom=room)
                        lesson.group_subject = group_subject
                        lesson.save()
                        room.is_free = False
                        room.save()
                        created = True
                        break
                if created:
                    break
            else:
                raise Exception("CAN NOT CREATE LESSON")


def get_lesson(group, day, time):
    obj = Lesson.objects.filter(free_classroom__time=time,
                                 free_classroom__day_of_week=day,
                                 group_subject__groups=group).first()
    return obj.to_dict() if obj else ''

