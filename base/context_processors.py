from django.core.urlresolvers import resolve


def get_current_url_name(request):

    return {
        'current_url_name': resolve(request.path_info).url_name
    }